package controle;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import modelo.Partida;

public class ControlePartida {
	public boolean insertPartida(Partida partida) {
		boolean retorno = false;
		try {
			Conexao com = new Conexao();
			PreparedStatement ps = com.getCom().prepareStatement("INSERT INTO partida(local,adv,time,juiz) VALUES(?,?,?,?);");
			ps.setString(1, partida.getLocal());
			ps.setString(2, partida.getAdv());
			ps.setString(3, partida.getTime());
			ps.setString(4, partida.getJuiz());
			if(!ps.execute()) {
				retorno = true;
			}
			com.FecharConexao();
		}catch(SQLException e) {
			System.out.println("Erro de SQL:"+e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro geral:"+e.getMessage());
		}
		return retorno;
	}
	
	public ArrayList<Partida> consultarTime(){
		ArrayList<Partida> lista = null;
		try {
			Conexao com = new Conexao();
			PreparedStatement ps = com.getCom().prepareStatement("SELECT * FROM partida;");
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				lista = new ArrayList<Partida>();
				while(rs.next()) {
					Partida partida = new Partida();
					partida.setId(rs.getInt("id"));
					partida.setLocal(rs.getString("local"));
					partida.setAdv(rs.getString("adv"));
					partida.setTime(rs.getString("time"));
					partida.setJuiz(rs.getString("juiz"));
					lista.add(partida);
				}
			}
			com.FecharConexao();
		}catch(SQLException e) {
			System.out.println("Erro de SQL: " + e.getMessage());
		}
		return lista;
	}
	
	public boolean deletPartida(Partida partida) {
		boolean retorno = false;
		try {
			Conexao com = new Conexao();
			PreparedStatement ps = com.getCom().prepareStatement("DELETE FROM partida WHERE time=?");
			ps.setString(1, partida.getTime());
			if(!ps.execute()) {
				retorno = true;
			}
			com.FecharConexao();
		}catch(SQLException e) {
			System.out.println("Erro de SQL:"+e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro geral:"+e.getMessage());
		}
		return retorno;
	}

	public boolean updateTime(Partida par) {
		boolean retorno = false;
		try {
			Conexao com = new Conexao();
			PreparedStatement ps = com.getCom().prepareStatement("UPDATE partida SET local=?, adv=?, juiz=? WHERE time=?;");
			ps.setString(1, par.getLocal());
			ps.setString(2, par.getAdv());
			ps.setString(3, par.getJuiz());
			ps.setString(4, par.getTime());
			if(!ps.execute()) {
				retorno = true;
			}
			com.FecharConexao();
		}catch(SQLException e) {
			System.out.println("Erro de SQL:"+e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro geral:"+e.getMessage());
		}
		return retorno;
	}

	
	
}
