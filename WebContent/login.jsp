<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
        <meta charset="UTF-8">
		<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' />
		<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0'>
       <title>Login</title>

        <link rel='stylesheet' type='text/css' href='Visual/css/semantic.css'>
		<link rel='stylesheet' type='text/css' href='Visual/css/custom.css'>
		<link rel='stylesheet' type='text/css' href='Visual/components/icon.css'>
		<link rel='stylesheet' type='text/css' href='Visual/css/visual.css'>
        <link href="Visual/imagens">
         <title>Login</title>
</head>
<body class='bodyy'>

		<div class='ui large top fixed hidden menu'>
		<div class='ui container'>
			<a class='active item' href='index.jsp'>Home</a>
			<div class='right menu'>
				<a class='item' href='login.jsp'>Login</a>
				<a class='item' href='cadastro.jsp'>Cadastre-se</a>
				<a class='item' href='Relatorio.jsp'>Relátorio</a>
			</div>
		</div>
	</div>

	<!-- menu mobile -->
	<div class='ui vertical inverted sidebar menu' style='background-color: #080b34;'>
		<a class='active item' href='index.jsp'>Home</a>
		<a class='item' href='login.jsp'>Login</a>
		<a class='item' href='cadastro.jsp'>Cadastrar</a>
		<a class='item' href='Relatorio.jsp'>Relátorio</a>
	</div>
	
	<!-- menu principal -->
	<div class='pusher'>  <!-- inicio menu pusher -->
		<div class='ui masthead'>
			<div>
				<div class='ui large secondary pointing menu'>
					<a class='toc item'>
						<i class='sidebar icon' style='color: black;'></i>
					</a>
					<a class='active item' href='index.jsp'>Home</a>
					<div class='right menu'>
						<a class='item' href='login.jsp'>Login</a>
						<a class='item' href='cadastro.jsp'>Cadastrar</a>
						<a class='item' href='Relatorio.jsp'>Relátorio</a>
					</div>
				</div>
			</div>
		</div>
		<br /><br />
	<div class='ui middle aligned center aligned grid'>
	<div class='column'>
	<div class='ui container'>
       <div class="header">
			<h3 class="ui center aligned header" id='rr'>Report Sport</h3>  
       </div>
        <div class=" content">
        	<form class="ui form" method="post" action="ServUsuario" >
        		<div class='ui stacked segment'>
					<div class='for'>
		            	<div class="field">
					        <div class='fieldd'>
					          <label>Email</label>
					          <div class="ui icon input">
					            <input type="text"  name="email"placeholder="Email">
					            <i aria-hidden="true" class="at icon"></i>
					          </div>
					       </div>
					     </div>
					     <div class="field">
					        <div class='fieldd'>
					          <label>Senha</label>
					          <div class="ui icon input">
					            <input type="text"  name="senha"placeholder="Email">
					            </div><i class="lock icon"></i>
					          </div>
					      </div>
					   
					  <input type='submit' class='ui large submit button' style='background-color: #080b34;color: white;' value='Logar' />
					 </div>
			     </div> 
			 </form>
			</div>
	</div>
	</div>
	</div>
	</div> <!-- fim menu pusher -->
</body>
<script src='Visual/js/jquery.min.js'></script>
<script src='Visual/js/semantic.js'></script>
<script src='Visual/components/visibility.js'></script>
<script src='Visual/components/sidebar.js'></script>
<script src='Visual/components/transition.js'></script>
<script src='Visual/components/modal.min.js'></script>	
<script src='Visual/js/teste.js'></script>
</html>