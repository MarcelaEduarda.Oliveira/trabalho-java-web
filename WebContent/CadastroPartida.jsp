<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' />
	<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0'>
	<!-- Site Properties -->
	<title>Projeto Java EE</title>
	
	<link rel='stylesheet' type='text/css' href='Visual/css/semantic.css'>
	<link rel='stylesheet' type='text/css' href='Visual/css/custom.css'>
	<link rel='stylesheet' type='text/css' href='Visual/components/icon.css'>
	<link rel='stylesheet' type='text/css' href='Visual/css/visual.css'>
	
</head>
<body class='body'>
	<!-- menu flutuante -->
	<div class='ui large top fixed hidden menu'>
		<div class='ui container'>
			<a class='active item' href='index.jsp'>Home</a>
			<div class='right menu'>
				<a class='item' href='login.jsp'>Login</a>
				<a class='item' href='cadastro.jsp'>Cadastre-se</a>
				<a class='item' href='Relatorio.jsp'>Relátorio</a>
			</div>
		</div>
	</div>
	
	<!-- menu mobile -->
	<div class='ui vertical inverted sidebar menu' style='background-color: #080b34;'>
		<a class='active item' href='index.jsp'>Home</a>
		<a class='item' href='login.jsp'>Login</a>
		<a class='item' href='cadastro.jsp'>Cadastrar</a>
		<a class='item' href='Relatorio.jsp'>Relátorio</a>
	</div>

	<!-- menu principal -->
	<div class='pusher'>  <!-- inicio menu pusher -->
		<div class='ui masthead'>
			<div>
				<div class='ui large secondary pointing menu'>
					<a class='toc item'>
						<i class='sidebar icon' style='color: black;'></i>
					</a>
					<a class='active item' href='index.jsp'>Home</a>
					<div class='right menu'>
						<a class='item' href='login.jsp'>Login</a>
						<a class='item' href='cadastro.jsp'>Cadastrar</a>
						<a class='item' href='Relatorio.jsp'>Relátorio</a>
					</div>
				</div>
			</div>
		</div>
	<br />

	<div class='ui middle aligned center aligned grid'>
	<div class='column'>
		<h1 style='color: white;text-shadow: 0.2em 0.2em 0.3em black;'>
			Informações da Partida e dos Jogadores
		</h1>		
		<form class='ui large form' action='ServPartida' method='post'>
			<div class='ui stacked segment'>
				<div class='for'>
				<div class='ui form'>
				  <div class='fields'>
				    <div class='field'>
				      <label>Local:</label>
				      <input type='text' name='local'>
				    </div>
				    <div class='field'>
				      <label>Time Adversário:</label>
				      <input type='text' name='adv'>
				    </div>
				    <div class='field'>
				      <label>Nome do juiz:</label>
				      <input type='text' name='juiz'>
				    </div>
				  </div>
				<div class='ui form'>
				  <div class='fields'>
				    <div class='field'>
				      <label>Camisa 1:</label>
				      <input type='text' >
				    </div>
				    <div class='field'>
				      <label>Camisa 2:</label>
				      <input type='text' >
				    </div>
				    <div class='field'>
				      <label>Camisa 3:</label>
				      <input type='text'>
				    </div>
				  </div>
				</div>
				<div class='ui form'>
				  <div class='fields'>
				    <div class='field'>
				      <label>Camisa 4:</label>
				      <input type='text' >
				    </div>
				    <div class='field'>
				      <label>Camisa 5:</label>
				      <input type='text' >
				    </div>
				    <div class='field'>
				      <label>Camisa 6:</label>
				      <input type='text'>
				    </div>
				  </div>
				</div>
				<div class='ui form'>
				  <div class='fields'>
				    <div class='field'>
				      <label>Camisa 7:</label>
				      <input type='text' >
				    </div>
				    <div class='field'>
				      <label>Camisa 8:</label>
				      <input type='text' >
				    </div>
				    <div class='field'>
				      <label>Camisa 9:</label>
				      <input type='text'>
				    </div>
				  </div>
				</div>
				<div class='ui form'>
				  <div class='fields'>
				    <div class='field'>
				      <label>Camisa 10:</label>
				      <input type='text' >
				    </div>
				    <div class='field'>
				      <label>Camisa 11:</label>
				      <input type='text' >
				  	</div>
				  </div>
				  </div>
				 </div>
				 <br />
				<input type='submit' class='ui large submit button' style='background-color: #080b34;color: white;' value='Cadastrar Informações' />
			</div>
			</div>	
		</form>
	</div>
	</div>

  	</div> <!-- fim menu pusher -->
  		
</body>
<script src='Visual/js/jquery.min.js'></script>
<script src='Visual/js/semantic.js'></script>
<script src='Visual/components/visibility.js'></script>
<script src='Visual/components/sidebar.js'></script>
<script src='Visual/components/transition.js'></script>
<script src='Visual/components/modal.min.js'></script>	
<script src='Visual/js/teste.js'></script>
</html>