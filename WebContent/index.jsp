
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
  
 <!DOCTYPE html>
	<html>
	<head>
		<meta charset='UTF-8'>
		<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' />
		<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0'>
		
		<!-- Site Properties -->
		<link rel='stylesheet' type='text/css' href='Visual/css/semantic.css'>
		<link rel='stylesheet' type='text/css' href='Visual/css/custom.css'>
		<link rel='stylesheet' type='text/css' href='Visual/components/icon.css'>
		<title>Home</title>
		
	</head>
	<body>
	
		<!-- menu flutuante -->
		<div class='ui large top fixed hidden menu'>
			<div class='ui container'>
				<a class='active item' href='index.jsp'>Home</a>
				<div class='right menu'>
					<a class='item' href='login.jsp'>Login</a>
					<a class='item' href='cadastro.jsp'>Cadastre-se</a>
				</div>
			</div>
		</div>

		<!-- menu mobile -->
		<div class='ui vertical inverted sidebar menu' style='background-color: #080b34;'>
			<a class='active item' href='index.php'>Home</a>
			<a class='item' href='login.jsp'>Login</a>
			<a class='item' href='cadastro.jsp'>Cadastrar</a>
		</div>

		<!-- menu principal -->
		
		<div class='pusher'>  <!-- inicio menu pusher -->

			<div class='ui masthead'>

				<div>
					<div class='ui large secondary pointing menu'>
						<a class='toc item'>
							<i class='sidebar icon' style='color: black;'></i>
						</a>
						<a class='active item' href='index.jsp'>Home</a>
						<div class='right menu'>
							<a class='item' href='login.jsp'>Login</a>
							<a class='item' href='cadastro.jsp'>Cadastrar</a>
						</div>
					</div>
				</div>
			</div>
		
		<header>
			<img class='ui fluid image' src='Visual/imagens/Report.png'>
		</header>
		
		<div class='ui vertical stripe segment'>
    		<div class='ui text container'>
     			<h3 class='ui header'>Para que serve o ReportSport?</h3>
      			<p>Se você quer deixar armazenado de maneira segura, e ter um feadback sobre o jogo do  time que você lidera, esse é o sistema perfeito para você!Ele te ajuda a deixar as informações da partida organizada e faz um relátorio detralhado para você ter controle do jogos!</p>
      		 </div>  			
  		</div>
  		<div class='ui vertical stripe segment'>
    		<div class='ui middle aligned stackable grid container'>
      			<div class='row'>
        			<div class='eight wide column'>
          				<h3 class='ui header'>Como funciona?</h3>
          				<p>É bem intuitivo e rápido!Primeiro você vai ter que informarqual a sua função(Apoio, técnico ou auxiliar) de acordo ncom sua função vai ser dirigido ao cadastro do time, informações do jogo e afins, no final, um relátorio vai ser gerado e Técnico e o auxiliar vão poder visualizar de maneira geral tudo sobre a partida!</p>
          				<h3 class='ui header'>Qual a finalidade do relátorio?</h3>
          				<p>Ajuda ao Técnico e o auxiliar a ter uma visão mais ampla e detalhada sobre a partida!</p>
        			</div>
       	 			<div class='six wide right floated column'>
          				<img src='Visual/imagens/bola.jpg' >
       	 			</div>
      			</div>
      			<div class='row'>
        			<div class='center aligned column'>
          				<a class='ui huge button' style='background-color: #080b34; color: #FFFFFF;'>Cadastrar</a>
        			</div>
      			</div>
    		</div>
  		</div>
  		<div class='ui container' >
		<div class='ui cards'>
			  <div class='card'>
			    <div class='content'>
			      <div class='header'>Apoio</div>
			      <div class='meta'>Responsável por:</div>
			      <div class='description'>
			      	Cadastrar Informações da Partida, e também os jogadores.
			      </div>
			    </div>
			  </div>
			  <div class='card'>
			    <div class='content'>
			      <div class='header'>Aúxiliar Técnico</div>
			      <div class='meta'>Responsável por:</div>
			      <div class='description'>
			        Cadastrar Informações da partida, classificar os pontos feitos durande a partida e também visualizar o Relatório da partida, bem como editar caso necessário.
			      </div>
			    </div>
			  </div>
			  <div class='card'>
			    <div class='content'>
			      <div class='header'>Técnico</div>
			      <div class='meta'>Responsável por:</div>
			      <div class='description'>
			        Visualizar Relatório final da partida, bem como editar as informações caso necessário.
			      </div>
			    </div>
			  </div>
			</div>
		</div>
		<br /><br /><br /><br />
  		</div> <!-- fim menu pusher -->
  		
  		<!-- footer -->
		<div class='ui inverted vertical footer segment ' style='background-color: #080b34;'>
			<div class='ui container'>
				<div class='ui stackable inverted divided equal height stackable grid'>
					<div class='three wide column'>
						<h4 class='ui inverted header'>Altores</h4>
						<div class='ui inverted link list'>
							<a  class='item'>Marcela Eduarda</a>
							<a  class='item'>Kailanne</a>
							<a  class='item'>Kauã Duarte</a>
							
						</div>
					</div>
					<div class='three wide column'>
						<h4 class='ui inverted header'>Redes Socias</h4>
						<div class='ui inverted link list'>
							<a href='#' class='item'><i class='facebook icon'></i> Facebook</a>
							<a href='#' class='item'><i class='instagram icon'></i> Instagram</a>
							<a href='#' class='item'><i class='twitter icon'></i> Twitter</a>
						</div>
					</div>
					<div class='seven wide column'>
						<img src='img/icon.png' alt='Imagem da logo' class='ui small image'>
						<h4 class='ui inverted header'>ReportSport</h4>
						
					</div>
				</div>
			</div>
		</div>
	
  		
	</body>
<script src='Visual/js/jquery.min.js'></script>
<script src='Visual/js/semantic.js'></script>
<script src='Visual/components/visibility.js'></script>
<script src='Visual/components/sidebar.js'></script>
<script src='Visual/components/transition.js'></script>
<script src='Visual/components/modal.min.js'></script>	
<script src='Visual/js/teste.js'></script>
</html>